# *SQL*

- SQL is a standard language for storing,  manipulating and retrieving data in databases.SQL is the most common language for extracting and organising data that is stored in a relational database. 

# *Features*

- High Performance

- High Availability

- Scalability and Flexibility

- High Security

# SQL Aggregate Functions 
  
 
- COUNT  

- SUM

- MIN 

- MAX

- AVG 
## Order TABLE

Order &nbsp;DetailID&nbsp; OrderID &nbsp;	ProductID&nbsp; Quantity &nbsp;<br/>
1	&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;10248  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;11  &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;12&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2<br/>
2	&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;10248	&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;42	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;10 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 3<br/>
3	&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;10248	&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;72	&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;5 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;  6<br/>
4	&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;10249	&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;14	&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;9 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  3<br/>
5	&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;10249  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;51	&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;40 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  1<br/>

&nbsp;&nbsp;


## COUNT

 COUNT() &nbsp;&nbsp; function returns the number of rows that matches a specified criterion.

    SELECT COUNT(ProductID)
    FROM Products;
    
    OUPUT : 5


## SUM
 
 SQL statement sum()  sum of the "Quantity" fields in the "OrderDetails" table:
   
    SELECT SUM(Quantity)
    FROM OrderDetails;

    OUTPUT :15

## AVG
  SQL statement finds the average price of all products:


    SELECT AVG(Quantity)
    FROM OrderDetails;

    OUTPUT : 3

## MIN
The following SQL statement finds the minimun quantity from the order table:    

    SELECT min(Quantity)
    FROM OrderDetails;

    OUTPUT : 1

## Max
The following SQL statement finds the minimun quantity from the order table    

    SELECT max(Quantity)
    FROM OrderDetails;

    OUTPUT : 6

#  Join

A JOIN clause is used to combine rows from two or more tables, based on a related column between them.

*Types of SQL JOINs*
- INNER JOIN 
- LEFT OUTER JOIN
- RIGHT OUTER JOIN
- FULL  JOIN 

## *INNER JOIN*
 
  The INNER JOIN keyword selects records that have matching values in both tables.

    SELECT column_name(s) 
    FROM table1
    INNER JOIN table2
    ON table1.column_name = table2.column_name;

## *LEFT OUTER JOIN*

  The LEFT JOIN keyword returns all records from the left table (table1), and the matched records from the right table (table2). The result is NULL from the right side, if there is no match.

    SELECT column_name(s)
    FROM table1
    LEFT JOIN table2
    ON table1.column_name = table2.column_name;

 ## *RIGHT JOIN*

  The RIGHT JOIN keyword returns all records from the right table (table2), and the matched records from the left table (table1). The result is NULL from the left side, when there is no match.

     SELECT column_name(s)
     FROM table1
     RIGHT JOIN table2
     ON table1.column_name = table2.column_name;

 ## *OUTER JOIN*

  The FULL OUTER JOIN keyword returns all records when there is a match in left (table1) or right (table2) table records.

- FULL OUTER JOIN can potentially return very large result-sets!

- FULL OUTER JOIN and FULL JOIN are the same

      SELECT column_name(s)
      FROM table1
      FULL OUTER JOIN table2
      ON table1.column_name = table2.column_name
      WHERE condition;




# *Why use SQL Database*

SQL will help you to easily get the information from data at high efficiency. With the help of SQL queries, you can view update events, monitor table, and database activity, identify specific data at time intervals and retrieve the information based on the requirement.

SQL commands such as "Select", "Insert", "Update", "Delete", "Create", and "Drop" can be used to accomplish almost everything that one needs to do with a database.

 ## *Select*

    SELECT * FROM table_name;


## *Drop*

The DROP DATABASE statement is used to drop an existing SQL database.

Syntax

    DROP DATABASE databasename;


# *Database management systems that use SQL are*
  - Oracle
  - Sybase
  - Microsoft SQL Server 
  - Access
  - Ingres 
  

## *Oracle*

Structured Query Language (SQL) is the set of statements with which all programs and users access data in an Oracle database. Application programs and Oracle tools often allow users access to the database without using SQL directly, but these applications in turn must use SQL when executing the user's request.

## Microsoft SQL Server*

Microsoft SQL Server is a relational database management system developed by Microsoft. As a database server, it is a software product with the primary function of storing and retrieving data as requested by other software applications—which may run either on the same computer or on another computer across a network.


# *References*

- [Orcale](https://docs.oracle.com/cd/B19306_01/server.102/b14200/intro.htm)
- [Microsoft SQL Server](https://www.microsoft.com/en-in/sql-server/sql-server-2019)
- [w3schools](https://www.w3schools.com/sql/sql_join.asp)
- [tutorialspoint](https://www.tutorialspoint.com/ms_sql_server/index.htm)
